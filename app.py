from flask import Flask
from flaskext.mysql import MySQL
from flask import request, jsonify

app = Flask(__name__)

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'flaskdb'
app.config['MYSQL_DATABASE_HOST'] = '127.0.0.1'

mysql.init_app(app)


@app.route('/api/users')
def users():
    try:
        cur = mysql.connect().cursor()
        cur.execute('SELECT * FROM `user`')
        return jsonify(cur.fetchall())
    except Exception as e:
        return(str(e))




def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


@app.route('/api/doc')
def home():
    inserComment = get_head_line("INSERT COMMENT :") + get_ptag_line("\n endpoint : /api/v1/comment/add \n mehod: POST \n "
                                                                     "Header: Content-Type:application/json \n"
                                                                     '{comment : book is very helpul, \n cat_id : 17}')
    getAllBook=get_head_line("GET ALL BOOKS :")+get_ptag_line("\n endpoint : /api/books/all \n mehod: GET \n \n")


    return getAllBook +inserComment


def get_head_line(h):
    return "<h1>"+h+"</h1>"

def get_ptag_line(p):
    return "<p>"+p+"</p>"



@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404
@app.errorhandler(500)
def serverError(e):
    return "<h1>500</h1><p> server error. "+e+"</p>", 500

@app.route('/api/books/all')
def getAllBooks():
    try:
        conn = mysql.connect();
        cur = conn.cursor()
        cur.execute('SELECT * FROM `books`')
        rv = cur.fetchall()
        #return str(rv)
        # # cur = mysql.get_db().cursor()
        # all_books = cur.execute('SELECT * FROM `books`').fetchall()
        return jsonify(rv)
    except Exception as e:
        return str(e)


@app.route("/api/books/add", methods=['POST'])
def addBook():
    try:
        conn = mysql.connect();
        sts_resp = request.get_json()
        books=sts_resp['books'];
        query = "INSERT INTO books(id, title, author) VALUES ";
        for book in books:
            id = str(book['id'])
            title = book['title']
            author = book['author']
            query+="("+id+",'"+title+"', '"+author+"'),"
        query=query[:-1]
        # return str(query)
        cur = conn.cursor()
        results = cur.execute(query);
        conn.commit()
        if results > 0:
           return jsonify({'message': 'data update success'})
        else:
           return jsonify({'message': 'data update failure'})

    except Exception as e:
        return (str(e))

@app.route("/api/book/delete")
def deleteBookByID():
    try:
        conn = mysql.connect();
        sts_resp = request.get_json()
        books=sts_resp['books'];
        query = "INSERT INTO books(id, title, author) VALUES ";
        for book in books:
            id = str(book['id'])
            title = book['title']
            author = book['author']
            query+="("+id+",'"+title+"', '"+author+"'),"
        query=query[:-1]
        # return str(query)
        cur = conn.cursor()
        results = cur.execute(query);
        conn.commit()
        if results > 0:
           return jsonify({'message': 'data update success'})
        else:
           return jsonify({'message': 'data update failure'})

    except Exception as e:
        return (str(e))
@app.route('/api/v1/comment/add', methods=['POST'])
def addComment():
    try:
        conn=mysql.connect();
        sts_resp = request.get_json()
        cat_id = str(sts_resp['cat_id'])
        comment = sts_resp['comment']
        if cat_id and comment is not None:
            insertQuery = "INSERT INTO comments(comment, cat_id) VALUES ('" + comment + "'," + cat_id + ")"
            #return str(insertQuery)
            if not (cat_id or comment):
                return page_not_found(404)
            cur = conn.cursor()
            results = cur.execute(insertQuery);
            conn.commit()
            if results==1:
                return jsonify({'message': 'data update success'})
            else:
                return jsonify({'message': 'data update failure'})
        else:
            return jsonify({'Invalid': 'request'})
    except Exception as e:
        return serverError(e)



if __name__ == '__main__':
    app.run(debug=True)
